/*
Input

The input contains a single integer 𝑎
(0≤𝑎≤63

).
Output

Output a single number.
https://codeforces.com/contest/1331/problem/C
*/
#include <bits/stdc++.h>
    
using namespace std;
    
int main() {
    int a;
    int i;
    scanf("%d",&a);
    vector<int> b(6);
    for (i = 0; i < 6; i++) {
    b[i] = (a >> i) & 1;
    }
    vector<int> c(6);
    c[4] = b[0];
    c[1] = b[1];
    c[3] = b[2];
    c[2] = b[3];
    c[0] = b[4];
    c[5] = b[5];
    int d = 0;
    for (i = 0; i < 6; i++) {
    d |= (c[i] << i);
    }
    printf("%d\n",d);
    return 0;
}