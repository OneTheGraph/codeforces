//
// Created by main on 19.04.2021.
//
// https://codeforces.com/problemset/problem/71/A
//
#include <iostream>

using namespace std;
int main() {
  int t, n;
  string input;
  cin >> t;
  while(t--){
    cin >> input;
    if(input.length() > 10) {
      cout << input[0] << input.length() - 2 << input[input.length() - 1] << endl;
    } else {
      cout << input << endl;
    }
  }
}
