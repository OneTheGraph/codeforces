  //
  // Created by main on 12.04.2021.
  //
  #include <iostream>

  using namespace std;
  int main() {
    int n, k, count;
    char symbol_count;
    cin >> n;
    cin >> k;
    symbol_count = 'a';
    count = 1;
    for(int i = 0; i < n; i++) {
      if(count > (n / k)) {
        symbol_count++;
        if(symbol_count == (k + 'a'))
          symbol_count = 'a';
        count = 1;
      }
      cout << symbol_count;
      count++;
    }
    return 0;
  }
