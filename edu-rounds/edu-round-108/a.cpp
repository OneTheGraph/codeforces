#include <iostream>

using namespace std;
int main() {
    int t, r, b, d, min;
    cin >> t;
    while (t--) {
	cin >> r;
	cin >> b;
	cin >> d;
	// r++;
	// b++;
	// d++;
	if (d == 0) {
	    if (r == b) {
		cout << "YES" << endl;
	    } else {
		cout << "NO" << endl;
	    }
	} else {
	    if (r > b) {
		if (r / d <= b) {
		    cout << "YES" << endl;
		} else {
		    cout << "NO" << endl;
		}
	    } else {
		if (b / d <= r) {
		    cout << "YES" << endl;
		} else {
		    cout << "NO" << endl;
		}
	    }
	}
    }
    return 0;
}
