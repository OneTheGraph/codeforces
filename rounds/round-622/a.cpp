#include <iostream>
//#include <algorithm>

using namespace std;

int main(){
    int a,b,c,t,res = 0;

    cin >> t;
    for(int i = 0; i<t; i++){
        cin >> a;
        cin >> b;
        cin >> c;
        /// Sort a > b > c
        if (a < b) swap(a, b);
        if (a < c) swap(a, c);
        if (b < c) swap(b, c);

        /// Take 1 disk
        if (c) res++, c--;
        if (b) res++, b--;
        if (a) res++, a--;
        /// Take 2 disks
        if (a && b) res++, a--, b--;
        if (a && c) res++, a--, c--;
        if (b && c) res++, b--, c--;
        /// Take 3 disks
        if (a && b && c) res++, a--, b--, c--;

        cout << res << endl;

        a = 0;
        b = 0;
        c = 0;
        res = 0;
    }
    return 0;
}